import UIKit

protocol Document{
    var name: String {get set}
    
    func createDocument()
    func description() -> String
    func ext() -> String
}

class HTML: Document{
    
    var name: String
    
    init(DocumentName documentname: String) {
        self.name = documentname
    }
    
    func createDocument() {
        print("document \(name) has been created" )
    }
    
    func description() -> String {
        return "HTML stands for Hyper Text Markup Language and is the standard markup language for Web pages."
    }
    
    func ext() -> String {
        return ".html"
    }
}

class JavaScript: Document{
    
    var name: String
    
    init(DocumentName documentname: String) {
        self.name = documentname
    }
    
    func createDocument() {
        print("document \(name) has been created" )
    }
    
    func description() -> String {
        return "JavaScript is the programming language of HTML and the Web."
    }
    
    func ext() -> String {
        return ".js"
    }
}

class Python: Document{
    
    var name: String
    
    init(DocumentName documentname: String) {
        self.name = documentname
    }
    
    func createDocument() {
        print("document \(name) has been created" )
    }
    
    func description() -> String {
        return "Python is a popular programming language. It was created by Guido van Rossum, and released in 1991."
    }
    
    func ext() -> String {
        return ".py"
    }
}

enum DocumentTypes{
    case Python
    case HTML
    case JavaScript
}

class DocumentFactory{
    private static var sharedDocumentFactory = DocumentFactory()
    
    class func shared() -> DocumentFactory {
        return sharedDocumentFactory
    }
    
    func getDocument(documenType: DocumentTypes, documentName: String) -> Document{
        switch documenType {
        case .HTML:
            return HTML(DocumentName: documentName)
        case .JavaScript:
            return JavaScript(DocumentName: documentName)
        case .Python:
            return Python(DocumentName: documentName)
        }
    }
}

let pythonDoc = DocumentFactory.shared().getDocument(documenType: .Python, documentName: "pythonTest")
pythonDoc.createDocument()

let htmlDoc = DocumentFactory.shared().getDocument(documenType: .HTML, documentName: "htmlTest")
htmlDoc.createDocument()

let jsDoc = DocumentFactory.shared().getDocument(documenType: .JavaScript, documentName: "JSTest")
jsDoc.createDocument()
