import UIKit

final class Singleton {
    static let sharedInstance = Singleton()
    
    private init() {
    }
    
    func checkInstance(){
        print("Instance checked")
    }
}

Singleton.sharedInstance.checkInstance()
