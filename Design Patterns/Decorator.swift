// Core Component
protocol Character {
    func getHealth() -> Int
}

// Concrete Components
struct Orc: Character {
    func getHealth() -> Int {
        return 10
    }
}

struct Elf: Character {
    func getHealth() -> Int {
        return 5
    }
}

// Decorator
protocol CharacterType: Character {
    var base: Character { get }
}

// Concrete Decorators
struct Warlord: CharacterType {

    let base: Character

    func getHealth() -> Int {
        return 50 + base.getHealth()
    }

    // New responsibility
    func battleCry() {
        print("RAWR")
    }
}

struct Epic: CharacterType {

    let base: Character

    func getHealth() -> Int {
        return 30 + base.getHealth()
    }
}

let orc = Orc()
let hp = orc.getHealth() // 10
let orcWarlord = Warlord(base: orc)
let hp = orcWarlord.getHealth() // 60
let epicOrcWarlord = Epic(base: orcWarlord)
let hp = epicOrcWarlord.getHealth() // 90
let doubleEpicOrcWarlord = Epic(base: epicOrcWarlord)
let hp = doubleEpicOrcWarlord.getHealth() // 120
let elf = Elf()
let hp = elf.getHealth() // 5
let elfWarlord = Warlord(base: elf)
let hp = elfWarlord.getHealth() // 55
elfWarlord.battleCry() // "RAWR"
