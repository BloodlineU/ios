import UIKit

var str = "Hello, playground"

protocol Car {
    func drive()
}
class Sedan: Car {
    func drive() {
        print("drive a sedan")
    }
}
class SUV: Car {
    func drive() {
        print("drive a SUV")
    }
}

class RedSedan: Sedan {
    override func drive() {
        print("drive a red sedan")
    }
}
protocol ColoredCar {
    var car: Car { get set }
    func drive()
}
class RedCar: ColoredCar {
    var car: Car
    init(car: Car) {
        self.car = car
    }
    func drive() {
        car.drive()
        print("It's red.")
    }
}

class BlueCar: ColoredCar {
    var car: Car
    init(car: Car) {
        self.car = car
    }
    func drive() {
        car.drive()
        print("It's blue.")
    }
}
//usage
let sedan = Sedan()
let redSedan = RedCar(car: sedan)
let suv = SUV()
let blueSUV = BlueCar(car: suv)
redSedan.drive()
blueSUV.drive()
